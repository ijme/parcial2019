/* base.db */

CREATE TABLE personas (
    dni INTEGER PRIMARY KEY ,
    nombre TEXT,
    apellido TEXT NOT NULL,
    categoria TEXT REFERENCES categorias(codigo)
);

CREATE TABLE categorias (
    codigo TEXT PRIMARY KEY ,
    descripcion TEXT NOT NULL
);