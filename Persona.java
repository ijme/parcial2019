public class Persona {
    private String dni;
    private String nombre;
    private String apellido;
    private String categoria;

    public Persona(String dni, String nombre, String apellido, String categoria){
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;  
        this.categoria = categoria;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getApellido(){
        return this.apellido;
    }
    
    public String getDni(){
        return this.dni;
    }
    
    public String getCategoria(){
        return this.categoria;
    }
}
