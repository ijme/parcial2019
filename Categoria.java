public class Categoria {
    private String codigo;
    private String descripcion;

    public Categoria (String unCodigo, String unaDescripcion){
        this.codigo = unCodigo;
        this.descripcion = unaDescripcion;
    }

    public String getCodigo(){
        return codigo;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public String toString(){
        return codigo + " - " + descripcion;
    }
}
    