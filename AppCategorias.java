import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import java.io.IOException;

public class AppCategorias extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("categorias.fxml")));
        stage.setTitle("Sistema");
        stage.setScene(scene);
        stage.show();
    }

    
    public static void main(String[] args){
        launch(args);
    }
}