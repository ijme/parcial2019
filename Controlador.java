import java.net.URL;
import java.util.ResourceBundle;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.cell.PropertyValueFactory;

public class Controlador {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField txtDNI;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtApellido;

    @FXML
    private ChoiceBox<Categoria> cbCategoria;

    @FXML
    private TableView<Persona> tblPersonas;

    @FXML
    void onClickGuardar(ActionEvent event) {
        String dni = this.txtDNI.getText();
        String nombre = this.txtNombre.getText();
        String apellido = this.txtApellido.getText();
        String categoria = this.cbCategoria.getSelectionModel().getSelectedItem().getCodigo();
        System.out.println("Guardando a " + apellido + "...");

        String sql = "INSERT INTO personas (dni, nombre, apellido, categoria) VALUES (?,?,?,?)";
        Base base = new Base("base.db");
        String respuesta = base.ejecutar(sql, dni, nombre, apellido, categoria);
        if(respuesta != ""){
            Alert alert = new Alert(AlertType.INFORMATION, respuesta);
            alert.setHeaderText(null);
            alert.showAndWait();
        }

        this.mostrarDatosTabla();
    }

    @FXML
    void initialize() {
        assert txtDNI != null : "fx:id=\"txtDNI\" was not injected: check your FXML file 'main.fxml'.";
        assert txtNombre != null : "fx:id=\"txtNombre\" was not injected: check your FXML file 'main.fxml'.";
        assert txtApellido != null : "fx:id=\"txtApellido\" was not injected: check your FXML file 'main.fxml'.";
        assert tblPersonas != null : "fx:id=\"tblPersonas\" was not injected: check your FXML file 'ventana.fxml'.";
        assert cbCategoria != null : "fx:id=\"cbCategoria\" was not injected: check your FXML file 'ventana.fxml'.";

        this.tblPersonas.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("dni"));
        this.tblPersonas.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.tblPersonas.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("apellido")); 
        this.tblPersonas.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("categoria"));

        this.mostrarCategorias();
        this.mostrarDatosTabla();
    }

    public void mostrarCategorias(){
        ObservableList<Categoria> categorias = FXCollections.observableArrayList();
        Base base = new Base("base.db");
        ArrayList<HashMap> registros = base.consultar("SELECT codigo, descripcion FROM categorias;");
        for(HashMap registro: registros){
            Categoria c = new Categoria((String)registro.get("codigo"), (String)registro.get("descripcion"));
            categorias.add(c);
        }
        this.cbCategoria.setItems(categorias);
    }

    public void mostrarDatosTabla(){
        ObservableList<Persona> personas = FXCollections.observableArrayList();
        Base base = new Base("base.db");
        ArrayList<HashMap> registros = base.consultar("SELECT dni, nombre, apellido, categoria FROM personas;");
        for(HashMap registro: registros){
            Persona p = new Persona((String)registro.get("dni"), (String)registro.get("nombre"), (String)registro.get("apellido"), (String)registro.get("categoria"));
            personas.add(p);
        }
        this.tblPersonas.setItems(personas);
    }
}