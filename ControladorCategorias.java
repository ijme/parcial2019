import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ControladorCategorias {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField txtCodigo;

    @FXML
    private TextField txtDescripcion;

   @FXML
    void obBtnGuardar(ActionEvent event) {
        String codigo = this.txtCodigo.getText();
        String descripcion = this.txtDescripcion.getText();
        System.out.println("Guardando categoria");

        String sql = "INSERT INTO categorias (codigo, descripcion) VALUES (?,?)";
        Base base = new Base("base.db");
        String respuesta = base.ejecutar(sql, codigo, descripcion);
        if(respuesta != ""){
            Alert alert = new Alert(AlertType.INFORMATION, respuesta);
            alert.setHeaderText(null);
            alert.showAndWait();
        }
    }
    @FXML
    void initialize() {
        assert txtCodigo != null : "fx:id=\"txtCodigo\" was not injected: check your FXML file 'categorias.fxml'.";
        assert txtDescripcion != null : "fx:id=\"txtDescripcion\" was not injected: check your FXML file 'categorias.fxml'.";

    }
}
